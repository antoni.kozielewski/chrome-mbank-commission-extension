// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';

let antReplace = document.getElementById('antReplace');
/**
chrome.storage.sync.get('color', function(data) {
  antReplace.style.backgroundColor = data.color;
  antReplace.setAttribute('value', data.color);
});

*/
antReplace.onclick = function(element) {
  let color = element.target.value;
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.executeScript(
        tabs[0].id,
        {code: 'chrome.storage.sync.get("commission", function(data) { ' +
		' var multiplier = data.commission; ' +
		' console.log("mbank commission fee multiplier = " + multiplier); ' + 
		'var numberOfRows = document.getElementsByClassName("k-master-row ng-star-inserted").length; '  +
		'for (i=0; i<numberOfRows; i++) { ' +		
		' var intValue = document.getElementsByClassName("styled-number__transition t-wallet-grid__cell--integer")[i].innerText.replace(" ",""); ' +
		' var fractionValue = document.getElementsByClassName("styled-number__transition t-wallet-grid__cell--fraction")[i].innerText.replace(",","."); ' +
		' var value = intValue + fractionValue; ' +
		' console.log("value = " + value); ' +		
		' console.log(" multiplier = " + multiplier); ' +
		' var comission = -1 * parseFloat(value) * multiplier; ' +
		' console.log("prowizja = " + comission.toFixed(2)); ' +
		' var e = Array.from(document.getElementsByClassName("t-grid-cell__currency t-grid-cell--red ng-star-inserted")).filter(e => e.innerText.includes("PLN") || e.innerText.includes("prowizja:"))[i]; ' +		
		' e.innerText = "prowizja: " + comission.toFixed(2); ' +
		' var e2 = Array.from(document.getElementsByClassName("l-grid-cell l-grid-cell--align-right")).filter(x => x.innerText.includes("PLN"))[(i+1) + i]; ' +
		' var minPrice = (1 + parseFloat(multiplier)) * parseFloat(e2.innerText.replace(",",".")) ; ' +  
		' e2.innerText = e2.innerText.replace("PLN", "__" + minPrice.toFixed(2)); ' +
		'} ' +
		'console.log("---===end===---"); ' +
		
		'});'});
  });
};
