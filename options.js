// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';

let button = document.getElementById('button');
let input = document.getElementById('inputValue');

function constructOptions() {
	console.log('input : ' + input);	
	chrome.storage.sync.get("commission", function(data) { input.value = data.commission; });    	
    button.addEventListener('click', function() {
	  var value = document.getElementById('inputValue').value;
	  console.log("new value is : " + value);
      chrome.storage.sync.set({commission: value}, function() {
        console.log('value now is : ' + value);
      })
    });    
}
constructOptions();
